#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


# Altering ls colors
LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=1;34:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:';
export LS_COLORS

alias ls='ls --color=auto'
PS1='[\u@ H\W]\$ '

function _update_ps1() {
    PS1=$(powerline-shell $?)
}

if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

# EXPORTS
export HOME=/home/qfrodicio
export EDITOR=micro
export QT_SELECT=5
export QT_STYLE_OVERRIDE=kvantum-dark

# GTK config

export GTK2_RC_FILES=/home/qfrodicio/.config/gtk-2.0/gtkrc 
export GTK_THEME=Arc:Dark

# File extraction

ex()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)     tar xjf $1    ;;
      *.tar.gz)      tar xzf $1    ;;
      *.bz2)         bunzip2 $1    ;;
      *.rar)         unrar x $1    ;;
      *.gz)          gunzip $1     ;;
      *.tar)         tar xf $1     ;;
      *.tbz2)        tar xjf $1    ;;
      *.tgz)         tar xzf $1    ;;
      *.zip)         unzip $1      ;;
      *.Z)           uncompress $1 ;;
      *.7z)          7z x $1       ;;
      *.deb)         ar x $1       ;;
      *.tar.xz)      tar xf $1     ;;
      *.tar,zst)     unzstd $1     ;;
      *)             echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


# ALIASES

# navigation
alias ..='cd ..'
alias .2='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# pacman and yay
alias pacupgrade='sudo pacman -Syyu'                   # Upgrade standard pkgs
alias yayupgrade='yay -Sua --noconfirm'                # Upgrade AUR pkgs
alias upgrade='yay -Syu --noconfirm'                   # Upgrade standard and AUR pkgs
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'      # remove orphaned packages
alias foreignpacupdate='pacman -Qqem > $HOME/gitlab/arch-dotfiles/pacman/foreignpkglist.txt'

# file listing
alias la='ls -a'
alias ll='ls -l'

# colorize grep output
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias status='git status'

# stow (for installing the dotfiles)
alias stowi='stow -t $HOME -S'
alias stowd='stow -t $HOME -D'

# ROS environment
source /opt/ros/noetic/setup.bash
source $HOME/catkin_ws/devel/setup.bash
export ROS_WORKSPACE=$HOME/catkin_ws

sh $HOME/keyboard_remap.sh

# Adding custom modules to the path
export PYTHONPATH="${PYTHONPATH}:/home/qfrodicio/PycharmProjects/speech_to_gesture/speech_to_gesture"

neofetch
