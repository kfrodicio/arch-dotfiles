export QT_SELECT=5
export QT_STYLE_OVERRIDE=kvantum-dark

# GTK config

export GTK2_RC_FILES=/home/qfrodicio/.config/gtk-2.0/gtkrc 
export GTK_THEME=Arc:Dark
