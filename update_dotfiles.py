#!/usr/bin/env python

from os import listdir, remove
from os.path import isfile, join, isdir, exists
import sys
import shutil


banned_dirs = ['.git', 'pacman']

def main(dotfiles_path):

    lvl0_items = listdir(dotfiles_path)
    lvl0_dirs = [i for i in lvl0_items if i not in banned_dirs]

    # Find the path to every file present in the dotfiles
    print("Updating file structure in dotfile project")

    file_struct = []
    
    for item in lvl0_dirs:
        updated_path = dotfiles_path + '/' + item
        file_struct += build_directory_tree(updated_path, '', [])

    # Remove the file currently in the dotfiles and replace it with the new version
    print("File structure updated")

    for item in file_struct:
        remove(item[1])
        shutil.copyfile(item[0], item[1])

    print("Files in dotfiles project updated")

def build_directory_tree(dotfile_path, conf_file_path, dir_tree):
    new_items = listdir(dotfile_path)
    for element in new_items:
        updated_dotfile_path = dotfile_path + '/' + element
        updated_conf_file_path = conf_file_path + '/' + element
        if isfile(updated_dotfile_path):
            if conf_file_path == '':
                updated_conf_file_path = '/home/qfrodicio/' + element
            dir_tree.append((updated_conf_file_path, updated_dotfile_path))
        elif isdir(updated_dotfile_path):
            if element in ['.config', '.xmonad'] and conf_file_path == '':
                updated_conf_file_path = '/home/qfrodicio/' + element
            dir_tree += build_directory_tree(updated_dotfile_path, updated_conf_file_path, dir_tree)

    return dir_tree


if __name__ == '__main__':
    main(sys.argv[1])
