import XMonad

import Data.Monoid
import System.IO
import System.Exit

-- Hook
import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers(doFullFloat, doCenterFloat, isFullscreen, isDialog)
import XMonad.Hooks.UrgencyHook

import XMonad.Config.Desktop
import XMonad.Config.Azerty

-- Utils
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig (additionalKeys, additionalMouseBindings)

-- Actions
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.SpawnOn

import qualified Codec.Binary.UTF8.String as UTF8

-- Layouts
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Layout.ResizableTile
import XMonad.Layout.NoBorders
import XMonad.Layout.Fullscreen (fullscreenFull)
import XMonad.Layout.Cross(simpleCross)
import XMonad.Layout.Spiral(spiral)
import XMonad.Layout.ThreeColumns
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.IndependentScreens
import XMonad.Layout.CenteredMaster(centerMaster)

import Graphics.X11.ExtraTypes.XF86
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import qualified Data.ByteString as B
import Control.Monad (liftM2)
import qualified DBus as D
import qualified DBus.Client as D

--Default programs
myTerminal      = "kitty"

myStartupHook = do
    spawn "$HOME/.xmonad/scripts/autostart.sh"
    setWMName "LG3D"

-- colours
normBord = "#000000"
focdBord = "#ff0000"
fore     = "#DEE3E0"
back     = "#282c34"
winType  = "#c678dd"

--mod4Mask= super key
--mod1Mask= alt key
--controlMask= ctrl key
--shiftMask= shift key

-- Dynamic Projects
projects :: [Project]
projects =
  [ Project { projectName        = "\xf17c"
            , projectDirectory   = "~/"
            , projectStartHook   = Nothing
            }

  , Project { projectName        = "\xf269"
            , projectDirectory   = "~/Downloads"
            , projectStartHook   = Just $ do spawn "firefox"
            }

  , Project { projectName         = "\xf3e2"
            , projectDirectory    = "~/Projects"
            , projectStartHook    = Nothing
            }

  , Project { projectName        = "\xf07c"
            , projectDirectory   = "~/"
            , projectStartHook   = Just $ do spawn "nemo"
            }

  , Project { projectName        = "\xf1bc"
            , projectDirectory   = "~/"
            , projectStartHook   = Just $ do spawn "spotify"
            }

  , Project { projectName        = "\xf008"
            , projectDirectory   = "~/Videos"
            , projectStartHook   = Just $ do spawn "kdenlive"
            }
            
  , Project { projectName        = "\xf2c6"
            , projectDirectory   = "~/"
            , projectStartHook   = Just $ do spawn "telegram-desktop"
            }
  ]


myModMask = mod4Mask
encodeCChar = map fromIntegral . B.unpack
myFocusFollowsMouse = True
myBorderWidth = 2

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

windowCount :: X (Maybe String) 
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myWorkspaces    = ["\xf17c","\xf269","\xf3e2","\xf07c","\xf1bc","\xf008", "\xf2c6"]
--myWorkspaces    = ["\61612","\61899","\61947","\61635","\61502","\61501","\61705","\61564","\62150","\61872"]
--myWorkspaces    = ["1","2","3","4","5","6","7","8","9","10"]
--myWorkspaces    = ["I","II","III","IV","V","VI","VII","VIII","IX","X"]

myBaseConfig = desktopConfig

-- window manipulations
myManageHook = composeAll . concat $
    [ [isDialog --> doCenterFloat]
    , [className =? c --> doCenterFloat | c <- myCFloats]
    , [title =? t --> doFloat | t <- myTFloats]
    , [resource =? r --> doFloat | r <- myRFloats]
    , [resource =? i --> doIgnore | i <- myIgnores]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61612" | x <- my1Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61899" | x <- my2Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61947" | x <- my3Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61635" | x <- my4Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61502" | x <- my5Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61501" | x <- my6Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61705" | x <- my7Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61564" | x <- my8Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\62150" | x <- my9Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61872" | x <- my10Shifts]
    ]
    where
    -- doShiftAndGo = doF . liftM2 (.) W.greedyView W.shift
    myCFloats = ["Arandr", "Arcolinux-calamares-tool.py", "Arcolinux-tweak-tool.py", "Arcolinux-welcome-app.py", "Galculator", "feh", "mpv", "Xfce4-terminal"]
    myTFloats = ["Downloads", "Save As..."]
    myRFloats = []
    myIgnores = ["desktop_window"]
    -- my1Shifts = ["Chromium", "Vivaldi-stable", "Firefox"]
    -- my2Shifts = []
    -- my3Shifts = ["Inkscape"]
    -- my4Shifts = []
    -- my5Shifts = ["Gimp", "feh"]
    -- my6Shifts = ["vlc", "mpv"]
    -- my7Shifts = ["Virtualbox"]
    -- my8Shifts = ["Thunar"]
    -- my9Shifts = []
    -- my10Shifts = ["discord"]


defaultGapSize = 3
defaultGaps = gaps [(U,defaultGapSize), (L,defaultGapSize), (D,defaultGapSize), (R,defaultGapSize)]
defaultSpaces = spacingRaw True (Border 3 3 3 3) True (Border 3 3 3 3) True
spacesAndGaps = defaultSpaces 

myLayout = smartBorders . avoidStruts $ spacesAndGaps $ tiled ||| Mirror tiled ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100



myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modMask, 1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modMask, 2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modMask, 3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))

    ]


myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm, xK_c), spawn $ "conky-toggle" )
    , ((modm, xK_f), sendMessage $ Toggle NBFULL)
    , ((modm, xK_r), spawn $ "rofi-theme-selector" )
    , ((modm, xK_v), spawn $ "pavucontrol" )
    , ((modm, xK_y), spawn $ "polybar-msg cmd toggle" )
    , ((modm, xK_x), spawn $ "archlinux-logout" ) 

    , ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)

    -- launch dmenu
    , ((modm,               xK_p     ), spawn "rofi -show drun -config ~/.config/rofi/themes/qfr_theme.rasi")

    -- launch gmrun
    , ((modm .|. shiftMask, xK_p     ), spawn "gmrun")

    -- close focused window
    , ((modm .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,               xK_s     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_a     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_a     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_s     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Spawn NetworkManager GUI

    , ((modm .|. shiftMask, xK_n     ), spawn "nm-connection-editor")

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    --SCREENSHOTS
      , ((0, xK_Print), spawn $ "scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")
      , ((controlMask, xK_Print), spawn $ "xfce4-screenshooter" )
      , ((controlMask .|. shiftMask , xK_Print ), spawn $ "gnome-screenshot -i")
      , ((controlMask .|. modm , xK_Print ), spawn $ "flameshot gui")
    
      --MULTIMEDIA KEYS
    
      -- Mute volume
      , ((0, xF86XK_AudioMute), spawn $ "amixer -q set Master toggle")
    
      -- Decrease volume
      , ((0, xF86XK_AudioLowerVolume), spawn $ "amixer -q set Master 5%-")
    
      -- Increase volume
      , ((0, xF86XK_AudioRaiseVolume), spawn $ "amixer -q set Master 5%+")
    
      -- Increase brightness
      , ((0, xF86XK_MonBrightnessUp),  spawn $ "xbacklight -inc 5")
    
      -- Decrease brightness
      , ((0, xF86XK_MonBrightnessDown), spawn $ "xbacklight -dec 5")
    
      -- Alternative to increase brightness
    
      -- Increase brightness
      -- , ((0, xF86XK_MonBrightnessUp),  spawn $ "brightnessctl s 5%+")
    
      -- Decrease brightness
      -- , ((0, xF86XK_MonBrightnessDown), spawn $ "brightnessctl s 5%-")
    
    --  , ((0, xF86XK_AudioPlay), spawn $ "mpc toggle")
    --  , ((0, xF86XK_AudioNext), spawn $ "mpc next")
    --  , ((0, xF86XK_AudioPrev), spawn $ "mpc prev")
    --  , ((0, xF86XK_AudioStop), spawn $ "mpc stop")
    
      , ((0, xF86XK_AudioPlay), spawn $ "playerctl play-pause")
      , ((0, xF86XK_AudioNext), spawn $ "playerctl next")
      , ((0, xF86XK_AudioPrev), spawn $ "playerctl previous")
      , ((0, xF86XK_AudioStop), spawn $ "playerctl stop")
    ]
    ++


  -- mod-[1..9], Switch to workspace N
  -- mod-shift-[1..9], Move client to workspace N
  [((m .|. modm, k), windows $ f i)

  --Keyboard layouts
  --qwerty users use this line
   | (i, k) <- zip (XMonad.workspaces conf) [xK_1,xK_2,xK_3,xK_4,xK_5,xK_6,xK_7,xK_8,xK_9,xK_0]

  --French Azerty users use this line
  -- | (i, k) <- zip (XMonad.workspaces conf) [xK_ampersand, xK_eacute, xK_quotedbl, xK_apostrophe, xK_parenleft, xK_minus, xK_egrave, xK_underscore, xK_ccedilla , xK_agrave]

  --Belgian Azerty users use this line
  --   | (i, k) <- zip (XMonad.workspaces conf) [xK_ampersand, xK_eacute, xK_quotedbl, xK_apostrophe, xK_parenleft, xK_section, xK_egrave, xK_exclam, xK_ccedilla, xK_agrave]

      , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)
      , (\i -> W.greedyView i . W.shift i, shiftMask)]]

  ++
  -- ctrl-shift-{w,e,r}, Move client to screen 1, 2, or 3
  -- [((m .|. controlMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
  --    | (key, sc) <- zip [xK_w, xK_e] [0..]
  --    , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

  [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
      | (key, sc) <- zip [xK_w, xK_e] [0..]
      , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty
    

main :: IO ()
main = do
    dbus <- D.connectSession
    -- Request access to the DBus name
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

    xmonad 

        $ewmh 
        $ docks 
        $ dynamicProjects projects
        $ defaults {logHook = dynamicLogWithPP (myLogHook dbus)}

-- Override the PP values as you would otherwise, adding colours etc... depending on the status bar used
myLogHook :: D.Client -> PP
myLogHook dbus = def
    { ppOutput = dbusOutput dbus
    , ppCurrent = wrap ("%{F" ++ "#ee1100" ++ "} ") " %{F-}"
    , ppVisible = wrap ("%{F" ++ "#ffffff" ++ "} ") " %{F-}"
    , ppUrgent = wrap ("%{F" ++  "#ffccff" ++ "} ") " %{F-}"
    , ppHidden = wrap ("%{F" ++ "#881100" ++ "} ") " %{F-}"
    , ppHiddenNoWindows = wrap ("%{F" ++ "#ffffff" ++ "} ") " %{F-}"
    , ppTitle = wrap ("%{F" ++ "#aaccff" ++ "} ") " %{F-}"
    , ppSep = " "
    , ppExtras = [windowCount]
    , ppOrder = \(ws:l:t:ex) -> [ws]	
    }

toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

-- Emit a DBus signal on log updates
dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
    	    D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
  where
    objectPath = D.objectPath_ "/org/xmonad/Log"
    interfaceName = D.interfaceName_ "org.xmonad.Log"
    memberName = D.memberName_ "Update"

defaults = def{
    -- simple stuff
      terminal           = myTerminal,
      focusFollowsMouse  = myFocusFollowsMouse,
      clickJustFocuses   = myClickJustFocuses,
      borderWidth        = myBorderWidth,
      modMask            = myModMask,
      workspaces         = myWorkspaces,
      normalBorderColor  = normBord,
      focusedBorderColor = focdBord,
    
    -- key bindings
      keys               = myKeys,
      mouseBindings      = myMouseBindings,
    
    -- hooks, layouts
      layoutHook         = myLayout,
      manageHook         = myManageHook,
      handleEventHook    = myEventHook,
      startupHook        = myStartupHook
}
